# spigot-docker
## What's this
Spigot server for Docker and docker-compose.

## Installation
1. `git clone git@gitlab.com:rkun/spigot-docker.git`
1. `cd spigot-docker`
1. `docker-compose build`
1. `mkdir data`
1. `echo "eula=true" > data/eula.txt`
1. `docker-compose up -d`

## Start
- `docker-compose up -d`

## Stop
**Run `/stop` commnand on your minecraft client before stop container!!**  
- `docker-compose down`
