FROM openjdk:8-jdk-alpine AS build

ENV SPIGOT_VER 1.15.2

WORKDIR /build
RUN apk --no-cache add git && \
wget "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar" -O BuildTools.jar && \
 java -jar BuildTools.jar –rev $SPIGOT_VER && \
 mkdir spigot && \
 cp spigot-${SPIGOT_VER}.jar spigot/spigot.jar


FROM openjdk:8-jre-alpine

RUN mkdir /data

WORKDIR /data
COPY --from=build /build/spigot /spigot

EXPOSE 25565
ENTRYPOINT java -jar /spigot/spigot.jar nogui
